<?php
    #This script check for username is exist or not
    function usernameCheck ($uname) {
        require_once('./db_connect.php');
        $conn = getConnection ();
        $sql = "SELECT uname FROM user WHERE uname collate utf8mb4_bin = ?;";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $uname);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = mysqli_fetch_assoc($result);

        if (($row["uname"]) === $uname) {
            return TRUE;
            #echo ("<i class='fas fa-check-circle text-success'></i>");
        }
        else {
            return FALSE;
            #echo ("<i class='fas fa-times-circle text-danger'></i> That username is not in our system.");
        }    
    }
?>