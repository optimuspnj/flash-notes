<?php
#This script updates notes list side panel
    function updateSidepane() {
        require_once('./db_connect.php');
        $conn = getConnection ();
        $count = 1;
        $sql = "SELECT * FROM note WHERE n_uname = ?;";
        $result = $conn->query($sql);
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $uname);
        $uname = $_COOKIE["cookie_flashnotes_lk_uname"];
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo "<button id='note".$count."' class='list-group-item list-group-item-action'>".$count.". ".$row["title"]."</button>";
                echo ("<script>$(document).ready(function(){");
                echo ("$('#note".$count."').click(function(){
                    $('#titlebox').val('".$row["title"]."');
                    $('#editorbox').val('".$row["content"]."');
                    $('#noteid').html('".$row["nid"]."');
                });");
                echo("});</script>");
                $count++;
            }
        }   
        else {
            echo "<a href='#' class='list-group-item list-group-item-action'>No saved notes</a>";
        }
    }
?>