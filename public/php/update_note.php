<?php
    #This script update existing notes
    require_once('./authorize_user.php');
    if (loginUser() == true) {

        require_once('./db_connect.php');
        $conn = getConnection ();

        $sql1 = "UPDATE note SET title=?, content=? WHERE nid=? AND n_uname=?;";
        $stmt1 = $conn->prepare($sql1);
        $stmt1->bind_param("ssis", $title, $content, $noteid, $noteuname);
        $title = ($_POST['titlebox']);
        $content = ($_POST['editorbox']);
        $noteid = ($_POST['noteid']);
        $noteuname = ($_POST['username']);
        
        if ($stmt1->execute() === TRUE) {
            $sql2 = "UPDATE modify_date SET date = ?, time = ? WHERE m_nid = ?;";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->bind_param("ssi", $date, $time, $noteid);
            date_default_timezone_set("Asia/Colombo");
            $date = date("Y-m-d");
            $time = date("H:i:s");
        } 

        if ($stmt2->execute() === TRUE) {
            require_once('./sidepane_update.php');
            updateSidepane();
        }  
        else {
            #echo "Error saving note: " . $conn->error;
            #Hidded the exception
        }
    }
    else {
        #Redirecting to login
        echo '<script type="text/javascript">window.location ="../login.html"</script>';
    }
?>