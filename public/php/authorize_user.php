<?php
    #Uses to login user using cookie and session data. Return TRUE if credentials are match
    session_start();
    function loginUser() {
        if(!isset($_COOKIE["cookie_flashnotes_lk_uname"])) {
            return false;
        }
        else {
            $sql = "SELECT uname FROM user WHERE uname collate utf8mb4_bin = ?;";
            require_once('./db_connect.php');
            $conn = getConnection ();
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("s", $uname);
            $uname = $_COOKIE["cookie_flashnotes_lk_uname"];
            $stmt->execute();
            $result = $stmt->get_result();
            $row = mysqli_fetch_assoc($result);

            if (($row["uname"]) === $uname) {
                $sql = "SELECT pass FROM user WHERE uname collate utf8mb4_bin = ?;";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param("s", $uname);
                $stmt->execute();
                $result = $stmt->get_result();
                $row = mysqli_fetch_assoc($result);
                $pswd = $_SESSION["session_flashnotes_lk_psswd"];
                
                if (($row["pass"]) === $pswd) { 
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
            $stmt->close();
            $conn->close();
        }
    }
?>