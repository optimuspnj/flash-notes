<?php
    #get the username status and send it to the reset page
    require_once('./username_check.php');
    if (usernameCheck ($_POST["uname"]) === TRUE) {
        echo ("<i class='fas fa-check-circle text-success'></i>");
    }
    else {
        echo ("<i class='fas fa-times-circle text-danger'></i> That username is not in our system.");
    }
?>