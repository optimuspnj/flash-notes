<?php
    #This script update user email
    session_start();
    require_once('./authorize_user.php');
    if (loginUser() == TRUE) {

        $pwd = ($_POST['pwd']);
        $pswd = $_SESSION["session_flashnotes_lk_psswd"];

        if ($pwd === $pswd) {
            require_once('./db_connect.php');
            $conn = getConnection ();

            $sql1 = "UPDATE user SET secq = ?, seca = ? WHERE user.uname = ?;";
            $stmt1 = $conn->prepare($sql1);
            $stmt1->bind_param("sss", $sques, $squesa, $uname);
        
            $sques = ($_POST['sques']);
            $squesa = ($_POST['squesa']);
            $uname = $_COOKIE["cookie_flashnotes_lk_uname"];

            if ($stmt1->execute() === TRUE) {
                echo ("<i class='fas fa-check-circle text-success'></i> Security question updated!");
            }  
            else {
                #echo "Error saving note: " . $conn->error;
                #Hidded the exception
            }
        }
        else {
            echo "<i class='fas fa-times-circle text-danger'></i> Wrong password";
        }
    }
    else {
        #Redirecting to login
        echo '<script type="text/javascript">window.location ="../login.html"</script>';
    }
?>