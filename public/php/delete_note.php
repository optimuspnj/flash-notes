<?php
    #This script delete existing notes
    require_once('./authorize_user.php');
    if (loginUser() === TRUE) {
        
        require_once('./db_connect.php');
        $conn = getConnection ();

        $sql1 = "DELETE FROM modify_date WHERE m_nid = ?;";
        $stmt1 = $conn->prepare($sql1);
        $stmt1->bind_param("i", $noteid);
        $noteid = ($_POST['noteid']);
        $noteuname = ($_POST['username']);

        if ($stmt1->execute() === TRUE) {
            $sql2 = "DELETE FROM note WHERE nid=? AND n_uname=?;";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->bind_param("is", $noteid, $noteuname);
        }  
        if ($stmt2->execute() === TRUE) {
            require_once('./sidepane_update.php');
            updateSidepane();
        }  
        else {
            #echo "Error deleting note: " . $conn->error;
            #Hidded the exception
        }
    }
    else {
        #Redirecting to login
        echo '<script type="text/javascript">window.location ="../login.html"</script>';
    }
?>