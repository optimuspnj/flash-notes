<?php
    #This script update user email
    require_once('./authorize_user.php');
    if (loginUser() == true) {

        require_once('./db_connect.php');
        $conn = getConnection ();

        $sql1 = "UPDATE user SET email = ? WHERE user.uname = ?;";
        $stmt1 = $conn->prepare($sql1);
        $stmt1->bind_param("ss", $email, $uname);
        $email = ($_POST['email']);
        $uname = $_COOKIE["cookie_flashnotes_lk_uname"];

        if ($stmt1->execute() === TRUE) {
            echo ("<i class='fas fa-check-circle text-success'></i> Email updated!");
        }  
        else {
            #echo "Error saving note: " . $conn->error;
            #Hidded the exception
        }
    }
    else {
        #Redirecting to login
        echo '<script type="text/javascript">window.location ="../login.html"</script>';
    }
?>