<?php
    #This script resets password when user forget it
    require_once('./db_connect.php');
    $conn = getConnection ();

    $sql = "SELECT seca FROM user WHERE uname = ?;";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $uname);
    $uname = ($_POST['uname']);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = mysqli_fetch_assoc($result);

    if ($result->num_rows > 0) {
        if ($_POST['secques'] === $row["seca"]) {
            if ($_POST['npass'] === $_POST['rpass']) {
                $sql1 = "UPDATE user SET pass = ? WHERE user.uname = ?";
                $stmt1 = $conn->prepare($sql1);
                $stmt1->bind_param("ss", $npwd, $uname);
                $npwd = $_POST['npass'];
                if ($stmt1->execute() === TRUE) {
                    $contentHtmlCode = "<h1 class='mt-3 text-center'><i class='fas fa-smile-wink'></i> Password reset complete.<br><small><i class='fas fa-check-circle text-success'></i> Your password has been updated.</small></h1><br><p class='text-center'><i class='fas fa-spinner'></i> Now you can <a class='text-success' href='../login.html'>login</a> with your new password.</p>";
                }  
                else {
                    #echo "Error reset passwd: " . $conn->error;
                    #Hidded the exception
                }
            }
            else {
                echo("passwords do not match");
            }
        }
        else {
            $contentHtmlCode = "<h1 class='mt-3 text-center'><i class='far fa-frown'></i> Error!<br><small><i class='fas fa-times-circle text-danger'></i> Your answer for security question is incorrect!</small></h1><br><p class='text-center'><i class='fas fa-undo-alt'></i> Please restart the process by visiting<a class='text-success' href='../pass_reset.html'> reset</a> page...</p>";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Flash Notes - Reset Password</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom.css">
    <script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <link rel="apple-touch-icon" sizes="180x180" href="../apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicon-16x16.png">
    <link rel="manifest" href="../site.webmanifest">
    <link rel="mask-icon" href="../safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <style>
        body {
            background: linear-gradient(60deg, #e3f3e2 50%, #8ed498 50%) no-repeat;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-light navbar-expand-sm bg-light">
    <a class="navbar-brand" href="../index.html">
        <img src="../img/logo.png" alt="FlashNotes" style="height:60px;">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="ml-auto navbar-nav">
            <li class="nav-item">
                <a class="btn btn-secondary" href="./login.html"><i class="fas fa-sign-in-alt"></i> Login</a>
                <a class="btn btn-success" href="./register.html"><i class="fas fa-plus-circle"></i> Register</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container mt-5">
    <div class="row">
        <div class="col-sm-12 border border-success rounded-lg mx-3">
            <?php echo $contentHtmlCode; ?>
        </div>
    </div>
</div>
<footer class="mt-5 sticky-top py-4 bg-dark">
    <div class="container text-center bg-dark">
        <small class="text-white">Copyright &copy; Flash Notes (2020)</small>
    </div>
</footer>
</body>
</html>