<?php
    #This script update user email
    session_start();
    require_once('./authorize_user.php');
    if (loginUser() == TRUE) {

        $pwd = ($_POST['opwd']);
        $pswd = $_SESSION["session_flashnotes_lk_psswd"];
        $npwd = ($_POST['npwd']);
        $pwdr = ($_POST['pwdr']);

        if ($pwd === $pswd) {
            if ($npwd === $pwdr) {
                if ($pswd === $npwd) {
                    require_once('./db_connect.php');
                    $conn = getConnection ();

                    $sql1 = "UPDATE user SET pass = ? WHERE user.uname = ?";
                    $stmt1 = $conn->prepare($sql1);
                    $stmt1->bind_param("ss", $npwd, $uname);
    
                    $uname = $_COOKIE["cookie_flashnotes_lk_uname"];

                    if ($stmt1->execute() === TRUE) {
                        echo ("<i class='fas fa-check-circle text-success'></i> Password updated sucessfully!");
                    }  
                    else {
                        #echo "Error saving note: " . $conn->error;
                        #Hidded the exception
                    }
                }
                else {
                    echo ("<i class='fas fa-times-circle text-danger'></i> New password is same as old password!");
                }
            }
            else {
                echo "<i class='fas fa-times-circle text-danger'></i> New password do not match";
            }
        }
        else {
            echo "<i class='fas fa-times-circle text-danger'></i> Wrong password";
        }
    }
    else {
        #Redirecting to login
        echo '<script type="text/javascript">window.location ="../login.html"</script>';
    }
?>