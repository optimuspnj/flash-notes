<?php
    #This script create new notes
    require_once('./authorize_user.php');
    if (loginUser() === TRUE) {

        require_once('./db_connect.php');
        $conn = getConnection ();
        $sql1 = "INSERT INTO note (title, content, n_uname) VALUES (?, ?, ?);";
        $stmt1 = $conn->prepare($sql1);
        $stmt1->bind_param("sss", $title, $content, $noteuname);
        $title = ($_POST['title']);
        $content = ($_POST['content']);
        $noteuname = ($_POST['username']);

        if ($stmt1->execute() === TRUE) {
            $sql2 = "INSERT INTO modify_date (m_nid, date, time) VALUES (?, ?, ?);";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->bind_param("iss", $latest_id, $date, $time);

            $latest_id =  mysqli_insert_id($conn); 
            date_default_timezone_set("Asia/Colombo");
            $date = date("Y-m-d");
            $time = date("H:i:s");
        }  

        if ($stmt2->execute() === TRUE) {
            require_once('./sidepane_update.php');
            updateSidepane();
        }  
        else {
            #echo "Error creating note: " . $conn->error;
            #Hidded the exception
        }
    }
    else {
        #Redirecting to login
        echo '<script type="text/javascript">window.location ="../login.html"</script>';
    }
?>