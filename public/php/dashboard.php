<!DOCTYPE html>
<html lang="en">
<head>
    <title>Flash Notes - Dashboard</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
    <link rel="stylesheet" href="../css/custom.css">

    <link rel="apple-touch-icon" sizes="180x180" href="../apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicon-16x16.png">
    <link rel="manifest" href="../site.webmanifest">
    <link rel="mask-icon" href="../safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <style>
        body {
            background: linear-gradient(60deg, #e3f3e2 50%, #8ed498 50%) no-repeat;
        }
        #editorbox,#titlebox {
            background: url("../img/noteedit-background.png") no-repeat center center;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
			-o-background-size: cover;
        }  
    </style>
</head>
<body>
<?php
    require_once('./authorize_user.php');
    if (loginUser() == true) {
        $uname = $_COOKIE["cookie_flashnotes_lk_uname"];
    }
    else {
        #Redirecting to login
        echo '<script type="text/javascript">window.location ="../login.html"</script>';
    }
?>

<nav class="navbar navbar-light navbar-expand-sm bg-light">
    <a class="navbar-brand" href="../index.html">
        <img src="../img/logo.png" alt="FlashNotes" style="height:60px;">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="ml-auto navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="../edit_account.html"><i class="fas fa-user"></i><?php echo(" ".$uname);?></a>
            </li>
            <li class="nav-item">
                <form action="./logout_process.php" method="post">
                    <button type="submit" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i> Logout</button>
                </form>
            </li>
        </ul>
    </div>
</nav>

<div class="container mt-5 bg-light rounded-lg">
<h1 class="mt-3"><i class="fas fa-tachometer-alt"></i> Dashboard</h1>
    <div class="row">
        <div class="col-sm-4 border border-success rounded-lg">
            <h2 class="mt-3"><i class="fas fa-copy"></i> Your notes</h2>
            <div class="list-group my-2" id="result">
                <?php
                    require_once('./sidepane_update.php');
                    updateSidepane();
                ?>
            </div> 
            <div class="list-group my-2">
                <form method="post" action="" id="createnoteform">
                    <button type="submit" class="btn btn-info btn-block" id="createnotebtn"><i class="fas fa-plus-square"></i> Create new <span id="create-btn-spinner" class="spinner-border-sm"></span></button>
                </form>
            </div> 
        </div>
        <div class="col-sm-8 border border-success rounded-lg">
            <h2 class="mt-3">Edit area</h2>
            <form method="post" action="" id="contactform">
                <div class="form-group">
                    <textarea readonly="true" class="form-control mb-3" id="titlebox" name="tbox" rows="1" placeholder="(Note title)"></textarea>
                </div>
                <div class="form-group">
                    <textarea readonly="true" class="form-control mb-3" id="editorbox" name="box1" rows="17" placeholder="(Select a note from left panel)"></textarea>
                </div>
                <button type="submit" class="btn btn-success mb-3" id="notesubmitbtn"><i class="fas fa-save"></i> Save <span id="save-btn-spinner" class="spinner-border-sm"></span></button>
                <button class="btn btn-primary mb-3" id="editbtn"><i class="fas fa-edit"></i> Edit</button>
                <button type="reset" class="btn btn-secondary mb-3" id="clearbtn"><i class="fas fa-backspace"></i> Clear</button>
                <button type="submit" class="btn btn-danger mb-3" id="notedeletebtn"><i class="fas fa-trash-alt"></i> Delete <span id="delete-btn-spinner" class="spinner-border-sm"></span></button>
            </form>
            <!--Using for submit note data-->
            <div class="invisible" id="noteid"></div>
            <div class="invisible" id="username"><?php echo($uname);?></div>
            <div class="invisible" id="notecount"></div>
        </div>
    </div>
</div>
<footer class="mt-5 sticky-top py-4 bg-dark">
    <div class="container text-center bg-dark">
        <small class="text-white">Copyright &copy; Flash Notes (2020)</small>
    </div>
</footer>
<script src="../js/dash_scripts.js"></script>
</body>
</html>