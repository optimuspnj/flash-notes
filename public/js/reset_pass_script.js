$(document).ready(function () { 
	$('#uname').focusout(function (e) {
        e.preventDefault();
        var uname = $('#uname').val();
		//$("#namechange-btn-spinner").addClass("spinner-border");
        $.ajax 
		({
			type: "POST",
			url: "./php/set_uname_status.php",
			data: { "uname": uname },
			success: function (data) {
				//$("#namechange-btn-spinner").removeClass("spinner-border");
		  		$('#server_response').html(data);
				if (data == "<i class='fas fa-check-circle text-success'></i>") {
					$.ajax 
					({
						type: "POST",
						url: "./php/set_securityqes.php",
						data: { "uname": uname },
						success: function (data) {
						//$("#namechange-btn-spinner").removeClass("spinner-border");
		  					$('#secquestion').html(data);
						}
					});
				}
			}
	  	});
	});
});