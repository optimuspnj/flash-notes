$(document).ready(function () {
	$('#notesubmitbtn').click(function (e) {
		e.preventDefault();
        var noteid = $("#noteid").html(); 
        var titlebox = $('#titlebox').val();
		var editorbox = $('#editorbox').val();
		var username = $("#username").html();
		$("#save-btn-spinner").addClass("spinner-border");
		$.ajax 
		({
			type: "POST",
			url: "../php/update_note.php",
			data: { "noteid": noteid, "titlebox": titlebox, "editorbox": editorbox, "username": username },
			success: function (data) {
				$("#save-btn-spinner").removeClass("spinner-border");
		  		$('#result').html(data);
			}
	  	});
	});
    $('#createnotebtn').click(function (e) {
		e.preventDefault();
    	var newtitle = "New Note";
		var newcontent = "New note content";
		var username = $("#username").html();
		$("#create-btn-spinner").addClass("spinner-border"); 
		$.ajax
	  	({
			type: "POST",
			url: "../php/create_note.php",
			data: { "title": newtitle, "content": newcontent, "username": username },
			success: function (data) {
		  		$('#result').html(data);
		  		$("#create-btn-spinner").removeClass("spinner-border");
			}
	  	});
  	});
  	$('#notedeletebtn').click(function (e) {
		e.preventDefault();
    	var noteid = $("#noteid").html();
		var username = $("#username").html();
		$("#delete-btn-spinner").addClass("spinner-border");
		$.ajax
		({
			type: "POST",
			url: "../php/delete_note.php",
			data: { "noteid": noteid, "username": username },
			success: function (data) {
			  	$('#result').html(data);
			  	$("#delete-btn-spinner").removeClass("spinner-border");
			}
		});
	});
    $('#editbtn').click(function(e){
        e.preventDefault();
        $("#titlebox").attr("readonly", false);
        $("#editorbox").attr("readonly", false);
        $('#clearbtn').click(function(){
            $('#editorbox').html('')
        });
    });
});