$(document).ready(function(){
    $("#fname").keyup(function(){
        if (this.value.length > 35 || this.value.length < 2) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else if (/[^a-zA-Z]/.test(this.value)) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else {
            $(this).addClass("is-valid");
            $(this).removeClass("is-invalid");
        }
    });
    $("#lname").keyup(function(){
        if (this.value.length > 35 || this.value.length < 2) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else if (/[^a-zA-Z]/.test(this.value)) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else {
            $(this).addClass("is-valid");
            $(this).removeClass("is-invalid");
        }
    });
    $("#uname").keyup(function(){
        if (this.value.length > 35 || this.value.length < 2) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else if (/[^a-zA-Z0-9]/.test(this.value)) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else {
            $(this).addClass("is-valid");
            $(this).removeClass("is-invalid");
        }
    });
    $("#email").keyup(function(){
        if (this.value.length > 50 || this.value.length < 5) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.value)) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else {
            $(this).addClass("is-valid");
            $(this).removeClass("is-invalid");
        }
    });
    var dropd = document.getElementById('sques');
    var selec = dropd.value;
    $("#sques").focusout(function(){
        if (selec === '') {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else {
            $(this).addClass("is-valid");
            $(this).removeClass("is-invalid");
        }
    });
    $("#squesa").keyup(function(){
        if (this.value.length > 35 || this.value.length < 2) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else if (/[^a-zA-Z0-9]/.test(this.value)) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else {
            $(this).addClass("is-valid");
            $(this).removeClass("is-invalid");
        }
    });
    $("#pwd").keyup(function(){
        if (this.value.length > 35 || this.value.length <= 8) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else if (/[^a-zA-Z0-9]/.test(this.value)) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else {
            $(this).addClass("is-valid");
            $(this).removeClass("is-invalid");
        }
    });
    var pwdin = document.getElementById('pwd');
    var pwd = pwdin.value;
    var pwdrin = document.getElementById('pwdr');
    var pwdr = pwdrin.value;
    $("#pwdr").keyup(function(){
        if (pwd !== pwdr) {
            $(this).addClass("is-invalid");
            $(this).removeClass("is-valid");
        }
        else {
            $(this).addClass("is-valid");
            $(this).removeClass("is-invalid");
        }
    });
});

// Disable form submissions if there are invalid fields
(function() {
    'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false || selec == '') {
            event.preventDefault();
            event.stopPropagation();
            alert("Please fill all the fields and agree to turms!");
          }
        }, false);
      });
    }, false);
})();
