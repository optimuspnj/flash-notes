$(document).ready(function () {
    $("#namebtn").click(function(){
        $("#nameedit").slideToggle("slow");
    });
    $("#emailbtn").click(function(){
        $("#emailedit").slideToggle("slow");
    });
    $("#secbtn").click(function(){
        $("#secquesedit").slideToggle("slow");
    });
    $("#passbtn").click(function(){
        $("#passedit").slideToggle("slow");
    });

	$('#namesubmit').click(function (e) {
        e.preventDefault();
        var fname = $('#fname').val();
        var lname = $('#lname').val();
		$("#namechange-btn-spinner").addClass("spinner-border");
        
        $.ajax 
		({
			type: "POST",
			url: "./php/edit_name.php",
			data: { "fname": fname, "lname": lname },
			success: function (data) {
				$("#namechange-btn-spinner").removeClass("spinner-border");
		  		$('#result1').html(data);
			}
	  	});
	});
	
	$('#emailsubmit').click(function (e) {
        e.preventDefault();
        var email = $('#email').val();
		$("#emailchange-btn-spinner").addClass("spinner-border");
        $.ajax 
		({
			type: "POST",
			url: "./php/edit_email.php",
			data: { "email": email },
			success: function (data) {
				$("#emailchange-btn-spinner").removeClass("spinner-border");
		  		$('#result2').html(data);
			}
	  	});
	});
	
	$('#secquessubmit').click(function (e) {
        e.preventDefault();
		var pwd = $('#currpwd').val();
		var sques = $('#sques').val();
		var squesa = $('#squesa').val();
		$("#secqueschange-btn-spinner").addClass("spinner-border");
        $.ajax 
		({
			type: "POST",
			url: "./php/edit_sques.php",
			data: { "pwd": pwd, "sques": sques, "squesa":squesa },
			success: function (data) {
				$("#secqueschange-btn-spinner").removeClass("spinner-border");
		  		$('#result3').html(data);
			}
	  	});
	});

	$('#passsubmit').click(function (e) {
        e.preventDefault();
		var opwd = $('#opwd').val();
		var npwd = $('#npwd').val();
		var pwdr = $('#pwdr').val();
		$("#passchange-btn-spinner").addClass("spinner-border");
        $.ajax 
		({
			type: "POST",
			url: "./php/edit_pass.php",
			data: { "opwd": opwd, "npwd": npwd, "pwdr":pwdr },
			success: function (data) {
				$("#passchange-btn-spinner").removeClass("spinner-border");
		  		$('#result4').html(data);
			}
	  	});
	});
});